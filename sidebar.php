    <!-- START Sidebar ######################################################## -->
    <div id="sidebar" class="three columns">

      <?php if(is_active_sidebar('sidebar-1')): dynamic_sidebar('sidebar-1'); endif; ?>
      <?php if(is_active_sidebar('sidebar-2')): dynamic_sidebar('sidebar-2'); endif; ?>
      <?php if(is_active_sidebar('sidebar-3')): dynamic_sidebar('sidebar-3'); endif; ?>
      
    </div>
    <!-- END Sidebar######################################################## -->
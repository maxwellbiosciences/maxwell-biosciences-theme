
  <?php 
    global $style_guide; 
    global $domain;
  ?>
  
  <div id="footer">
    <?php if(is_active_sidebar('footer-full')): ?>
    <div class="row">
       <?php dynamic_sidebar('footer-full'); ?>
    </div>
    <?php endif; ?>
    <div class="row">

      <?php if(is_active_sidebar('footer-6-1')): dynamic_sidebar('footer-6-1'); endif; ?>
      <?php if(is_active_sidebar('footer-9-1')): dynamic_sidebar('footer-9-1'); endif; ?>

      <?php if(is_active_sidebar('footer-3-1')): dynamic_sidebar('footer-3-1'); endif; ?>
      <?php if(is_active_sidebar('footer-3-2')): dynamic_sidebar('footer-3-2'); endif; ?>
      <?php if(is_active_sidebar('footer-3-3')): dynamic_sidebar('footer-3-3'); endif; ?>
      <?php if(is_active_sidebar('footer-3-4')): dynamic_sidebar('footer-3-4'); endif; ?>

      <?php if(is_active_sidebar('footer-6-2')): dynamic_sidebar('footer-6-2'); endif; ?>
      <?php if(is_active_sidebar('footer-9-2')): dynamic_sidebar('footer-9-2'); endif; ?>

    </div>
  </div>

  <div id="footer-branding">
    <div class="row">
      <?php
        $custom_logo_id = get_theme_mod( 'custom_logo' );
        $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
      ?>
      <img src="<?php echo $image[0]; ?>" height="30" alt="<?php bloginfo( 'name' ); ?>" />
      <?php if(is_active_sidebar('footer-branding')): dynamic_sidebar('footer-branding'); endif; ?>
    </div>
  </div>

  <div id="notification"><div id="notification-content"></div></div>
  <div id="alert">
    <div id="alert-window">
      <h4 id="alert-title"></h4>
      <p id="alert-content"></p>
      <div id="alert-control">
        <button type="submit" class="button c-transparent" id="alert-secondary"></button><button type="submit" class="button c-transparent" id="alert-primary"></button>
      </div>
    </div>
  </div>

  <!-- SCRIPT ######################################################## -->
  <script type="text/javascript" src="<?php echo $style_guide; ?>js/script.js"></script>
  <script type="text/javascript" src="<?php echo $style_guide; ?>sites/<?php echo $domain; ?>/script.js"></script>
  <?php if(is_wpe_snapshot()): // staging changes ?>
  <script type="text/javascript" href="<?php echo get_template_directory_uri(); ?>/script.js"></script>
  <?php endif; ?>
  <?php wp_footer(); ?>

</body>
</html>

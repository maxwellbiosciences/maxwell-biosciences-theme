<?php
  global $sidebar_active;
  global $style_guide;
  global $domain;
  global $show_header_image;
  $sidebar_active = (is_active_sidebar('sidebar-1') || is_active_sidebar('sidebar-2') || is_active_sidebar('sidebar-3'));
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

  <!-- BASIC & SEO ######################################################## -->
  <meta charset="utf-8">
  <title><?php if(is_front_page()){ echo get_bloginfo( 'name' ); ?> - <?php echo get_bloginfo("description"); }else{ wp_title(' - ', true, 'right'); } ?></title>

  <!-- META ######################################################## -->
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <link rel="profile" href="http://gmpg.org/xfn/11" />
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
  <?php wp_head(); ?>

  <!-- FONT ######################################################## -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/icon?family=Material+Icons" />

  <!-- CSS######################################################## -->
  <link rel="stylesheet" type="text/css" href="<?php echo $style_guide; ?>css/style.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $style_guide; ?>sites/<?php echo $domain; ?>/style.css" />

  <?php if(is_wpe_snapshot()): // staging changes ?>
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css" />
  <?php endif; ?>

</head>
<body>

  <!-- START Header ######################################################## -->
  <!-- ALTERNATIVE: data-video="static/videos/computer.mp4"> -->
  <?php $page_settings = get_post_meta($post->ID); ?>
  <div id="header" class="<?php if(isset($page_settings["header-size"]) && $show_header_image){ foreach($page_settings["header-size"] as $a){ echo $a . " "; }} ?>" style="color:<?php if(isset($page_settings["header-foreground-color"])){ echo $page_settings["header-foreground-color"][0]; }else{ ?>#333<?php } ?>;">

    <div class="background-element <?php if(has_post_thumbnail()){ echo "background "; } ?>" style="<?php if(has_post_thumbnail()){ ?>background-image:url(<?php the_post_thumbnail_url('full'); ?>);background-size:cover;background-position:center center;<?php } ?>">
      <?php if(has_post_video() && $show_header_image){ ?><div class="video"><video src="<?php echo get_the_post_video_url(); ?>" autoplay="autoplay" loop="loop"></video></div><?php } ?>
      <div class="overlay" style="background:<?php if(isset($page_settings["header-overlay-color"])){ echo $page_settings["header-overlay-color"][0]; }else{ ?>#fff<?php } ?>;opacity:<?php if(isset($page_settings["header-overlay-opacity"])){ echo $page_settings["header-overlay-opacity"][0]; }else{ ?>0<?php } ?>;"></div>
    </div>
    
    <div class="header-branding">
      <div class="header-branding-inner">
        <?php the_custom_logo(); ?>
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
          <span class="title"><?php bloginfo( 'name' ); ?></span>
        </a>
        <?php if(has_nav_menu('header-menu')): ?>
        <div id="menu">
          <div id="search"><input type="text" id="search-box" placeholder="Search" /><i id="search-button" class="material-icons">search</i></div>
          <?php wp_nav_menu( array( 'container' => 'nav', 'theme_location' => 'header-menu' ) ); ?>
          <div class="clear"></div>
        </div>
        <div id="menu-opener"><i class="material-icons">menu</i></div>
        <?php endif; ?>
      </div>
    </div>

    <?php if(is_active_sidebar('header') && (has_post_thumbnail() || has_post_video())): ?>
    <div class="section" <?php if(isset($page_settings["header-css"])){ ?>style="<?php echo $page_settings["header-css"][0]; ?>"<?php } ?>>
      <div class="header-content">
        <?php dynamic_sidebar('header'); ?>
      </div>
    </div>
    <?php endif; ?>

  </div>
  <!-- END Header ######################################################## -->
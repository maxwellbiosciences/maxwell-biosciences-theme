<?php
  get_header();
  $page_settings = get_post_meta($post->ID);
?>

<div id="page" class="<?php if(isset($page_settings["page-settings"])){ foreach($page_settings["page-settings"] as $a){ echo $a; } } ?>">

    <?php global $sidebar_active; ?>

    <!-- START Content ######################################################## -->
    <div id="content" <?php if($sidebar_active): ?>class="nine columns"<?php endif; ?>>

      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <!--<h2><?php the_title(); ?></h2>-->
      <?php the_content(); ?>
      <?php endwhile; else: ?><?php endif; ?>

    </div>
    <!-- END Content######################################################## -->

    <?php if($sidebar_active){ get_sidebar(); } ?>

    <div class="clear"></div>
  </div>

  <?php get_footer(); ?>
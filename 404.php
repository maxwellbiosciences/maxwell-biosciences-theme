
<?php get_header(); ?>

<div id="page" class="centered" style="height:40vh;">

        <h2>Sorry, this page doesn’t exist!</h2>
        <p>The link you followed may be broken, or this page may have been removed. <a href="/">Go back to <?php echo get_bloginfo( 'name' ); ?>.</a></p>

        <script type="text/javascript">
          ga("send", {
            hitType: 'event',
            eventCategory: "web",
            eventAction: "404",
            eventLabel: JSON.stringify({"occurance":window.location.href, "source":document.referrer, "ua":navigator.userAgent})
          });
        </script>

</div>

<?php get_footer(); ?>

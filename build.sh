
echo "Preparing folder:"
rm -rf ./.build
rm -rf ./maxwell-biosciences-theme.zip
mkdir ./.build
mkdir ./.build/maxwell-biosciences-theme

echo "Copy theme to build folder:"
rsync -av --progress ./* ./.build/maxwell-biosciences-theme --exclude ./.build/
rm -rf ./.build/maxwell-biosciences-theme/build.sh

#echo "Minifying theme":
#node ./.buildtools/build.js

echo "Packing theme:"
cd ./.build/
zip -r ../maxwell-biosciences-theme.zip ./maxwell-biosciences-theme
cd ..
rm -rf ./.build

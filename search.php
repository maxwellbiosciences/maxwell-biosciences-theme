<?php
  global $show_header_image;
  $show_header_image = false;
  get_header();
?>
<div id="page">

  <?php global $sidebar_active; ?>

  <!-- START Content ######################################################## -->
  <div id="content" <?php if($sidebar_active){ ?>class="nine columns"<?php } ?>>

    <?php
      global $wp_query;
    ?>
    <div class="c-light-gray" style="padding:10px;margin-bottom:60px;"><?php echo $wp_query->found_posts; ?> results for "<?php echo $wp_query->query["s"]; ?>"</div>
    <?php
      if($wp_query->have_posts()):
      while ( $wp_query->have_posts() ) {
        $wp_query->the_post();
        $category = get_the_category();
    ?>
    <h2><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
    <h6>Posted on <?php the_time('F jS, Y') ?> <?php // print_r(get_category_link($category[0]->term_id)); ?></h6>
    <p><?php $content = substr(strip_tags(get_the_content("", false)), 0, 300); echo $content . (strlen($content) < 300 ? "" : "..."); ?></p>
    <p><a href="<?php echo get_permalink(); ?>">Read More <i class="material-icons">chevron_right</i></a></p>
    <div class="new-section"></div>
    <?php
      }
      else:
    ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
    <?php endif; ?>

    <div class="centered"><div class="action pagination button-group">
      <?php echo str_replace(array("page-numbers", "current"), array("button button-primary", "c-disabled"), paginate_links(array("next_text" => "<i class=\"material-icons\">chevron_right</i>", "prev_text" => "<i class=\"material-icons\">chevron_left</i>"))); ?>
    </div></div>

  </div>
  <!-- END Content######################################################## -->

  <?php if($sidebar_active){ get_sidebar(); } ?>

  <div class="clear"></div>
</div>

<?php get_footer(); ?>
<?php

  // ###########################################################################
  // GENERAL FUNCTIONS
  // ###########################################################################
  // set basic settings
  $sidebar_active = false;
  $show_header_image = true;
  // get environment
  $style_guide = "https://maxwellbiosciences.com/static/v0/style-guide/";
  if(is_wpe_snapshot()){
    $style_guide = "https://maxwellbio.staging.wpengine.com/static/v0/style-guide/";
  }
  // find the base domain of the page
  $domain = get_site_url();
  $start = strpos($domain, "//") + 2;
  $end = strpos($domain, "/", $start);
  if($end <= 0) $end = strlen($domain);
  $domain = substr($domain, $start, $end - $start);

  // ###########################################################################
  // REMOVE MORE WP BULLSHIT
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  function tjnz_head_cleanup() {
      remove_action( 'wp_head', 'feed_links_extra', 3 );
      remove_action( 'wp_head', 'feed_links', 2 );
      remove_action( 'wp_head', 'rsd_link' );
      remove_action( 'wp_head', 'wlwmanifest_link' );
      remove_action( 'wp_head', 'index_rel_link' );
      remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
      remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
      remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
      remove_action( 'wp_head', 'wp_generator' );
      if (!is_admin()) {
        wp_deregister_script('jquery');
        wp_register_script('jquery', '', '', '', true);
        wp_deregister_script( 'wp-embed' );
      }
  }
  add_action('init', 'tjnz_head_cleanup');
  // remove WP version from RSS
  add_filter('the_generator', 'tjnz_rss_version');
  function tjnz_rss_version() { return ''; }

  // theme auto update
  $current_time = time();
  function theme_update_hook($transient){
    global $current_time;
    if( empty( $transient->checked['maxwell-biosciences-theme'] ) ){
      return $transient;
    }
    $current = file_get_contents("https://gitlab.com/maxwellbiosciences/maxwell-biosciences-theme/raw/master/style.css");
    $start = strpos($current, "Version:") + 8;
    $version = trim(substr($current, $start, strpos($current, "\n", $start) - $start));
    if( version_compare( $transient->checked['maxwell-biosciences-theme'], $version, '<' ) ){
      $transient->response['maxwell-biosciences-theme'] = array("new_version" => $version, "package" => "https://gitlab.com/maxwellbiosciences/maxwell-biosciences-theme/raw/master/maxwell-biosciences-theme.zip", "");
    }
    update_option("maxwell_biosciences_theme_update_check", $current_time);
    return $transient;
  }
  $check = get_option("maxwell_biosciences_theme_update_check");
  if(!$check || $current_time - $check >= 28800){
    add_filter("pre_set_site_transient_update_themes", "theme_update_hook");
  }
  /*function notify_update(){
    wp_mail("patrick@healthanon.org", "[WP]" .  get_title() . " - Site Updated", "This is just an automatic notification.");
  }
  wp_register_theme_activation_hook('maxwell-biosciences-theme', 'notify_update');*/

  //move scripts to footer
  function remove_head_scripts() { 
    remove_action('wp_head', 'wp_print_scripts'); 
    remove_action('wp_head', 'wp_print_head_scripts', 9); 
    remove_action('wp_head', 'wp_enqueue_scripts', 1);
    add_action('wp_footer', 'wp_print_scripts', 5);
    add_action('wp_footer', 'wp_enqueue_scripts', 5); 
    add_action('wp_footer', 'wp_print_head_scripts', 5);
  } 
  add_action( 'wp_enqueue_scripts', 'remove_head_scripts' );

  // remove the annoying WP behavior
  function tiny_mce_remove_unused_formats($init) {
    $init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 4=h4;Heading 5=h5;Block=div;Blockquote=blockquote;';
    return $init;
  }
  add_filter('tiny_mce_before_init', 'tiny_mce_remove_unused_formats' );

  // register menus
  function register_my_menu() {
    register_nav_menu('header-menu',__('Header Menu'));
    register_nav_menu('footer-menu',__('Footer Menu'));
  }
  add_action('init', 'register_my_menu');

  // add page logo
  add_theme_support('custom-logo', array('height' => 50, 'flex-width' => true));

  // add featured images
  add_theme_support( 'post-thumbnails' );

  // register widgets
  function arphabet_widgets_init(){
    // header widget
    register_sidebar(array(
      'name' => 'Header', 'id' => 'header',
      'before_widget' => '', 'after_widget'  => '', 'before_title'  => '<h1>', 'after_title'   => '</h1>',
    ));
    // sidebar widgets
    register_sidebar(array(
      'name'          => 'Sidebar 1',
      'id'            => 'sidebar-1',
      'before_widget' => '<div>',
      'after_widget'  => '</div>',
      'before_title'  => '<span>',
      'after_title'   => '</span>',
    ));
    register_sidebar(array(
      'name'          => 'Sidebar 2',
      'id'            => 'sidebar-2',
      'before_widget' => '<div>',
      'after_widget'  => '</div>',
      'before_title'  => '<span>',
      'after_title'   => '</span>',
    ));
    register_sidebar(array(
      'name'          => 'Sidebar 3',
      'id'            => 'sidebar-3',
      'before_widget' => '<div>',
      'after_widget'  => '</div>',
      'before_title'  => '<span>',
      'after_title'   => '</span>',
    ));
    // footer widgets
    register_sidebar(array(
      'name'          => 'Footer Grid 3 - 1', 'id'            => 'footer-3-1',
      'before_widget' => '<div class="columns three">', 'after_widget'  => '</div>', 'before_title'  => '<span>', 'after_title'   => '</span>',
    ));
    register_sidebar(array(
      'name'          => 'Footer Grid 3 - 2', 'id'            => 'footer-3-2',
      'before_widget' => '<div class="columns three">', 'after_widget'  => '</div>', 'before_title'  => '<span>', 'after_title'   => '</span>',
    ));
    register_sidebar(array(
      'name'          => 'Footer Grid 3 - 3', 'id'            => 'footer-3-3',
      'before_widget' => '<div class="columns three">', 'after_widget'  => '</div>', 'before_title'  => '<span>', 'after_title'   => '</span>',
    ));
    register_sidebar(array(
      'name'          => 'Footer Grid 3 - 4', 'id'            => 'footer-3-4',
      'before_widget' => '<div class="columns three">', 'after_widget'  => '</div>', 'before_title'  => '<span>', 'after_title'   => '</span>',
    ));
    register_sidebar(array(
      'name'          => 'Footer Grid 6 - 1', 'id'            => 'footer-6-1',
      'before_widget' => '<div class="columns six">', 'after_widget'  => '</div>', 'before_title'  => '<span>', 'after_title'   => '</span>',
    ));
    register_sidebar(array(
      'name'          => 'Footer Grid 6 - 2', 'id'            => 'footer-6-2',
      'before_widget' => '<div class="columns six">', 'after_widget'  => '</div>', 'before_title'  => '<span>', 'after_title'   => '</span>',
    ));
    register_sidebar(array(
      'name'          => 'Footer Grid 9 - 1', 'id'            => 'footer-9-1',
      'before_widget' => '<div class="columns nine">', 'after_widget'  => '</div>', 'before_title'  => '<span>', 'after_title'   => '</span>',
    ));
    register_sidebar(array(
      'name'          => 'Footer Grid 9 - 2', 'id'            => 'footer-9-2',
      'before_widget' => '<div class="columns nine">', 'after_widget'  => '</div>', 'before_title'  => '<span>', 'after_title'   => '</span>',
    ));
    register_sidebar(array(
      'name'          => 'Footer Grid Full', 'id'            => 'footer-full',
      'before_widget' => '<div>', 'after_widget'  => '</div>', 'before_title'  => '<span>', 'after_title'   => '</span>',
    ));
    register_sidebar(array(
      'name'          => 'Footer Branding', 'id'            => 'footer-branding',
      'before_widget' => '<div>', 'after_widget'  => '</div>', 'before_title'  => '', 'after_title'   => '',
    ));
  }
  add_action('widgets_init', 'arphabet_widgets_init');

  // remove readmore scroll
  function remove_more_link_scroll( $link ) {
    $link = preg_replace( '|#more-[0-9]+|', '', $link );
    return $link;
  }
  add_filter( 'the_content_more_link', 'remove_more_link_scroll' );

?>
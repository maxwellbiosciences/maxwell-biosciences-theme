<?php
/*
Template Name: Multi-Post
*/

  get_header();

?>
<div id="page">

    <?php global $sidebar_active; ?>

    <!-- START Content ######################################################## -->
    <div id="content" <?php if($sidebar_active){ ?>class="nine columns"<?php } ?>>

      <?php 
        $page_id = $post->ID;
        if (have_posts()) : while (have_posts()):
          the_post();
          if($post->ID == $page_id)
            continue;
      ?>
      <h2><?php the_title(); ?></h2>
      <h6>Posted on <?php the_time('F jS, Y') ?></h6>
      <?php the_content(__('Read More <i class="material-icons">chevron_right</i>')); ?>
      <div class="new-section"></div>
      <?php endwhile; else: ?>
      <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
      <?php endif; ?>

      <div class="centered"><div class="action pagination button-group">
        <?php echo str_replace(array("page-numbers", "current"), array("button button-primary", "c-disabled"), paginate_links(array("next_text" => "<i class=\"material-icons\">chevron_right</i>", "prev_text" => "<i class=\"material-icons\">chevron_left</i>"))); ?>
      </div></div>

    </div>
    <!-- END Content######################################################## -->

    <?php if($sidebar_active){ get_sidebar(); } ?>

    <div class="clear"></div>
  </div>

  <?php get_footer(); ?>
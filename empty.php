<?php
/*
Template Name: Empty
*/
?>

<?php get_header(); ?>

<div class="empty">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <?php the_content(); ?>
  <?php endwhile; else: ?><?php endif; ?>
</div>

<?php get_footer(); ?>
